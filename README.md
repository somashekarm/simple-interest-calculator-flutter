### Simple Interest Calculator Application

This repository contains the source code of a simple interest calculator developed using [flutter](https://flutter.dev/docs).


#### Requirements
1. [Android Studio Install](https://developer.android.com/studio/install)
2. [Flutter Install](https://flutter.dev/docs/get-started/install)
3. Dart
4. Android Device


#### UI

![SI Image Output UI](images/si1.png)   

![SI Image Output UI](images/si2.png)



#### Credits
Flutter Tutorial for Beginners using Dart: Develop Android and iOS mobile app by [Smartherd](https://www.youtube.com/playlist?list=PLlxmoA0rQ-Lw6tAs2fGFuXGP13-dWdKsB)
