import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/services.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "SI Calculator Application",
      home: Form(),
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.orange,
        accentColor: Colors.green,
      ),
    ));

class Form extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FormState();
  }
}

class _FormState extends State<Form> {
  var _formKey = GlobalKey<FormState>();

  var _currencies = ['Rupees', 'Pound', 'Dollars'];
  final double _minPadding = 5.0;

  var _currentItemSelected = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentItemSelected = _currencies[0];
  }

  var finalResult = '';

  TextEditingController principalAmountController = TextEditingController();
  TextEditingController rateOfInterestController = TextEditingController();
  TextEditingController termController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Simple Interest Calculator"),
      ),
      body: prefix0.Form(
        key: _formKey,
        child: Padding(
            padding: EdgeInsets.all(_minPadding * 2),
            child: ListView(
              children: <Widget>[
                getImageAsset(),
                Padding(
                    padding:
                        EdgeInsets.only(top: _minPadding, bottom: _minPadding),
                    child: TextFormField(
                      controller: principalAmountController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please Enter Principal amount";
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        labelText: 'Principal Amount',
                        hintText: 'Enter Principal Amount e.g. 10000',
                        errorStyle: TextStyle(fontSize: 15.0
                            //color: Colors.red
                            ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: _minPadding, bottom: _minPadding),
                    child: TextFormField(
                      controller: rateOfInterestController,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please Enter Rate of interest';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      //
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        labelText: 'Rate of Interest',
                        hintText: 'Enter percentage e.g. 10%',
                        errorStyle: TextStyle(fontSize: 15.0
                            //color: Colors.red
                            ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: _minPadding, bottom: _minPadding),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          controller: termController,
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please Enter term';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            labelText: 'Term',
                            hintText: 'Term in Years',
                            errorStyle: TextStyle(fontSize: 15.0
                                //color: Colors.red
                                ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                          ),
                        )),
                        Container(
                          width: _minPadding * 5,
                        ),
                        Expanded(
                            child: DropdownButton<String>(
                          items: _currencies.map((String dropDownMenuItem) {
                            return DropdownMenuItem<String>(
                              value: dropDownMenuItem,
                              child: Text(dropDownMenuItem),
                            );
                          }).toList(),
                          value: _currentItemSelected,
                          onChanged: (String newValueSelected) {
                            _onDropDownItemSelected(newValueSelected);
                          },
                        ))
                      ],
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: _minPadding, bottom: _minPadding),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            child: Text('Calculate'),
                            onPressed: () {
                              setState(() {
                                if (_formKey.currentState.validate()) {
                                  this.finalResult = _calculateTotalReturns();
                                }
                              });
                            },
                          ),
                        ),
                        Container(
                          width: _minPadding * 5,
                        ),
                        Expanded(
                          child: RaisedButton(
                            child: Text('Reset'),
                            onPressed: () => _resetTextFields()
                            ,
                          ),
                        ),
                      ],
                    )),
                Padding(
                  padding:
                      EdgeInsets.only(top: _minPadding, bottom: _minPadding),
                  child: Text(finalResult),
                )
              ],
            )),
      ),
    );
  }

  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/money_new.png');
    Image image = Image(
      image: assetImage,
      width: 125.0,
      height: 125.0,
    );

    return Container(
      child: image,
      margin: EdgeInsets.all(_minPadding * 10),
    );
  }

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
    });
  }

  String _calculateTotalReturns() {
    double principalAmount = double.parse(principalAmountController.text);
    double rateOfInterest = double.parse(rateOfInterestController.text);
    double term = double.parse(termController.text);

    double totalAmountReturns =
        principalAmount + (principalAmount * rateOfInterest * term) / 100;

    String result = 'After $term years, your investment will be worth '
        '$totalAmountReturns $_currentItemSelected';
    return result;
  }

  void _resetTextFields() {
    setState(() {
    principalAmountController.text = '';
    rateOfInterestController.text = '';
    termController.text = '';
    finalResult = '';
    _currentItemSelected = _currencies[0];
    });
  }
} // main
